import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    themes: {
      light: {
        primary: '#2196f3',
        secondary: '#f44336',
        accent: '#03a9f4',
        error: '#ff5722',
        warning: '#ff9800',
        info: '#4caf50',
        success: '#009688'
      },
    },
  },
});
