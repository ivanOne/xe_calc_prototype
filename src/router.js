import VueRouter from 'vue-router'

import DayProductsPage from './pages/DayProductsPage'
import ProductSelectorPage from './pages/ProductSelectorPage'

const routes = [
    { path: '/', component: DayProductsPage },
    { path: '/product_selector', component: ProductSelectorPage },
]

const router = new VueRouter({
    routes
})

export default router