const state = {
    snackbarGlobal: {show: false, text: '',  color: ''}
}

const mutations =  {
    showSnackbar(state, payload) {
        state.snackbarGlobal.color = payload.color
        state.snackbarGlobal.text = payload.text
        state.snackbarGlobal.show = true
    },

    hideSnackbar(state) {
        state.snackbarGlobal = {show: false, text: '',  color: ''}
    }
}

export default {
    namespaced: true,
    state,
    mutations
}