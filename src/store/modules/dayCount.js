const state = {
    breakfast: {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []},
    lunch: {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []},
    dinner: {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []},
    snack: {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []},
    xeTotal: 0,
    carbTotal: 0,
    caloriesTotal: 0
}

function calcXeCount(carbCount){
    let carbCountClean = parseInt(carbCount)
    let result = carbCountClean / 12
    return round(result)
}

function round(number){
    return Math.round(number * 100) / 100
}

const mutations =  {
    addProduct(state, payload) {
        if (['breakfast', 'lunch', 'dinner', 'snack'].includes(payload.dayTime)){
            let product = {}
            let portion = payload.portion
            let productXe = calcXeCount(portion.carbCount)

            product.name = payload.name
            product.portionName = portion.name
            product.portionValue = portion.value
            product.carbCount = portion.carbCount
            product.xeCount = productXe
            product.calories = portion.calories
            
            state[payload.dayTime].products.push(product)
            state[payload.dayTime].xeCount = round(state[payload.dayTime].xeCount + product.xeCount)
            state[payload.dayTime].carbCount = round(state[payload.dayTime].carbCount + product.carbCount)
            state[payload.dayTime].carbCount = round(state[payload.dayTime].caloriesCount + product.calories)

            state.xeTotal = round(productXe + state.xeTotal)
            state.carbTotal = round(product.carbCount + state.carbTotal)
            state.caloriesTotal = round(product.calories + state.caloriesTotal)
        }
    },

    removeProductFromDish(state, payload){
        if (['breakfast', 'lunch', 'dinner', 'snack'].includes(payload.dayTime)){
            let index = payload.index
            let dayTime = payload.dayTime
            let xeCount = state[dayTime].products[index].xeCount
            let carbCount = state[dayTime].products[index].carbCount
            let caloriesCount = state[dayTime].products[index].calories

            state[dayTime].products.splice(index, 1)
            state[dayTime].xeCount = round(state[dayTime].xeCount - xeCount)
            state[dayTime].carbCount = round(state[dayTime].carbCount - carbCount)
            state[dayTime].carbCount = round(state[dayTime].caloriesCount - caloriesCount)

            state.xeTotal = round(state.xeTotal - xeCount)
            state.carbTotal = round(state.carbTotal - carbCount)
            state.caloriesTotal = round(state.caloriesTotal - caloriesCount)
        }
    },

    cleanDish(state){
        state.breakfast = {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []}
        state.lunch = {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []}
        state.dinner = {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []}
        state.snack = {'xeCount': 0,'caloriesCount': 0,'carbCount': 0, 'products': []}
        state.xeTotal = 0
        state.carbTotal = 0
        state.caloriesTotal = 0
    }
}

export default {
    namespaced: true,
    state,
    mutations
}