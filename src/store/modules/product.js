const state = {
    userProducts: [],
    systemProducts: []
}

const mutations =  {
    setUserProducts(state, payload) {
        state.userProducts = payload.userProductList
    },

    setSystemProducts(state, payload) {
        state.systemProducts = payload.systemProductList
    },

    changeIsFeatureUserProduct(state, payload) {
        let userProductId = state.userProducts.findIndex(item => item.id == payload.id)
        let systemProductId = state.systemProducts.findIndex(item => item.id == payload.id)

        state.systemProducts[systemProductId].isFeature = false
        state.userProducts.splice(userProductId, 1)
    },

    changeIsFeatureSystemProduct(state, payload) {
        let systemProductId = state.systemProducts.findIndex(item => item.id == payload.id)
        let userProductId = state.userProducts.findIndex(item => item.id == payload.id)

        if (state.systemProducts[systemProductId].isFeature){
            state.systemProducts[systemProductId].isFeature = false
            state.userProducts.splice(userProductId, 1)
        }else{
            state.systemProducts[systemProductId].isFeature = true
            state.userProducts.push(state.systemProducts[systemProductId])
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations
}