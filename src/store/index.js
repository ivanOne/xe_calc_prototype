import Vue from 'vue'
import Vuex from 'vuex'

import dayCount from './modules/dayCount'
import product from './modules/product'
import serviceState from './modules/serviceState'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    dayCount,
    product,
    serviceState
  },
  strict: debug
})